 angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});  
  
  $scope.checkAdmin = function()
  {
	  if ($localStorage.isadmin == 1)
		  $scope.showAdminLink = true;
	  else
		  $scope.showAdminLink = false;	  
  }
  
  $scope.checkAdmin();

  
  $scope.logOut = function()
  {

	$localStorage.userid = '';
	$localStorage.appid = '';
	$localStorage.name = '';
	$localStorage.phone = '';
	$localStorage.email = '';
	$localStorage.image = '';
	$localStorage.isadmin = '';
				
	  window.location ="#/app/login";
  }
})




.controller('LoginCtrl', function($scope,$localStorage,$ionicPopup,$http,$rootScope,$ionicLoading,$ionicHistory,$state) 
{
	
    if ($localStorage.userid)
    {
        $ionicHistory.nextViewOptions({
                disableAnimate: true,
                expire: 300
            });
    
        $state.go('app.projects');
    }

	
	$scope.fields = 
	{
		"phone" : ""
	}
	
	$scope.checkLogin = function()
	{
		if ($scope.fields.phone =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין טלפון לזיהוי',
				 template: ''
			});	
		}
		else
		{
			
				$ionicLoading.show({
				  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
				});	
			
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

				send_params = 
				{
					"phone" : $scope.fields.phone
				}
				//console.log(login_params)
				$http.post($rootScope.LaravelHost+'/LoginUser',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicLoading.hide();
					
					if (data[0].status == 0)
					{
						$ionicPopup.alert({
							 title: 'טלפון זיהוי שגוי יש לנסות שוב',
							 template: ''
						});		

						$scope.fields.phone = '';
					}
					else
					{
						$localStorage.userid = data[0].userid;
						$localStorage.appid = data[0].appid;
						$localStorage.name = data[0].name;
						$localStorage.phone = data[0].phone;
						$localStorage.email = data[0].email;
						$localStorage.image = data[0].image;
						$localStorage.isadmin = data[0].isadmin;
						
						
						$scope.checkAdmin();
						if ($localStorage.isadmin == 1)	
							window.location ="#/app/projects";
						else
							window.location ="#/app/apartmentdetails/"+$localStorage.appid;
				
						
						
					}
				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});				
		}
		//window.location ="#/app/projects";
	}

})

.controller('ProjectCtrl', function($scope,$localStorage,$ionicPopup,$http,$rootScope,$ionicLoading) 
{
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.getProjects = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetProjects',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("projects: " , data);
				$scope.ProjectsArray = data;

			})
			.error(function(data, status, headers, config)
			{

			});	

			
	}
	
	$scope.getProjects();
	
	$scope.gotoProject = function(id,count,lastid,image)
	{
		
		//alert (count);
		//return;
		
		//ui-sref="app.buildings({ItemId: item.index})"
		//$rootScope.currentProject = $scope.ProjectsArray[id];
		$rootScope.selectedProjectNumber = id;
		$rootScope.projectImage = image;
		
		if (count == 1)
		{
			window.location ="#/app/apartments/"+lastid;
		}
		else
		{
			window.location ="#/app/buildings/"+id;
		}
	}
	
	$scope.deleteProject = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteProject',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.ProjectsArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}
	
})

.controller('BuildingCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicLoading) 
{
	$scope.projectId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.imagesArray = [];
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';


	$scope.navigateApparment = function(id)
	{
		//alert (id);
		window.location ="#/app/apartments/"+id;
	}
		
	$scope.getBuildings = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.projectId
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetBuildings',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("Buildings: " , data);
				$scope.BuildingsArray = data;

				
				if (data[0].gallery && data.length > 0)
					$scope.imagesArray = data[0].gallery;


			})
			.error(function(data, status, headers, config)
			{

			});	

			
	}
	
	$scope.getBuildings();
	
	
	$scope.deleteBuilding = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteBuilding',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.BuildingsArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });		
	}

	

})


.controller('ApartmentsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicLoading) 
{
	 
	
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.buildingid = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.projectImage = $rootScope.projectImage;
	
	$rootScope.buildingId = $scope.buildingid;

	//if ($rootScope.currentProject)
	//$scope.ProjectName = $rootScope.currentProject.name;
	

	$scope.GetApartments = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"buildingid" : $scope.buildingid
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetApartments',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("Apartments: " , data);
				$scope.ApartmentsArray = data;
				//$rootScope.ApartmentsArray = data;
			

			})
			.error(function(data, status, headers, config)
			{

			});	

			
	}
	
	$scope.GetApartments();
	
	
	$scope.deleteApp = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteApartments',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.ApartmentsArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}

})



.controller('ManageProjectCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$timeout,$cordovaCamera,$ionicLoading) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.projectid = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;	
	$scope.newimage = "";
	$scope.showDiv = false;
	
	$scope.fields = 
	{
		"name" : "",
		"address" : "",
		"phone" : "",
		"facebook" : "",
		"desc" : "",
		"image" : "",
		"serverimage" : "",
		"status" : ""
	}
	
	$scope.autocompleteOptions = {
	  componentRestrictions: { country: 'IL' }
	}


	$scope.navigateApparment = function(id)
	{
		//alert (id);
		window.location ="#/app/apartments/"+id;
	}

	
	$scope.GetSingleProject = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.projectid
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetSingleProject',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("GetSingleProject: " , data);
				
				$scope.fields.name = data[0].name;
				$scope.fields.address = data[0].address;
				$scope.fields.phone = data[0].phone;
				$scope.fields.facebook = data[0].facebook;
				$scope.fields.desc = data[0].desc;
				$scope.fields.status = data[0].status;
				
				$scope.fields.image = data[0].image;
				$scope.fields.serverimage = $rootScope.serverHost+data[0].image;
				//$scope.ApartmentsArray = data;
			

			})
			.error(function(data, status, headers, config)
			{

			});			
	}
	

	if ($scope.projectid == -1)
	{
		//$scope.navTitle = "הוספת פרויקט חדש";
		$scope.Url = 'NewProject';
	}
	else
	{
		//$scope.navTitle = "עריכת פרויקט";
		$scope.GetSingleProject();
		$scope.Url = 'EditProject';
		$scope.showDiv = true;

		
	}
	

	$scope.getBuildings = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.projectid
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetBuildings',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("Buildings: " , data);
				$scope.BuildingsArray = data;

				
				if (data[0].gallery && data.length > 0)
					$scope.imagesArray = data[0].gallery;


			})
			.error(function(data, status, headers, config)
			{

			});	

			
	}
	
	$scope.getBuildings();	
	
	
	$scope.deleteBuilding = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteBuilding',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.BuildingsArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });		
	}

	
	
	

	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	

	
	$scope.saveProject = function()
	{
		

		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם הפרויקט',
				 template: ''
			});				
		}
		else if ($scope.fields.address == "")
		{
			$ionicPopup.alert({
				 title: 'יש למלא כתובת הפרויקט',
				 template: ''
			});				
		}
		
		
		else if ($scope.fields.status == "")
		{
			$ionicPopup.alert({
				 title: 'יש לבחור סטטוס הפרויקט',
				 template: ''
			});				
		}

		/*
		else if ($scope.projectid == -1 && $scope.newimage =="")
		{
			$ionicPopup.alert({
				 title: 'יש לבחור תמונה',
				 template: ''
			});	
		}
		*/
		
			
		
		else
		{

			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			if ($scope.newimage)
				$scope.changedimage = $scope.newimage;
			else
				$scope.changedimage = $scope.fields.image;
				
			if ($scope.fields.address.formatted_address)	
				$scope.address = $scope.fields.address.formatted_address;
			else
				$scope.address = $scope.fields.address;
		
			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.projectid,
				"name" : $scope.fields.name,
				"address" : $scope.address,
				"phone" : $scope.fields.phone,
				"facebook" : $scope.fields.facebook,
				"desc" : $scope.fields.desc,
				"image" : $scope.changedimage,
				"status" : $scope.fields.status,

		
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{

				$rootScope.selectedProjectNumber = data[0].newid;
			
				if ($scope.projectid == -1) 
				{
					window.location ="#/app/managebuildings/"+$rootScope.selectedProjectNumber+"/-1";
				}
				else
				{
					window.location ="#/app/projects";
				}
			
				

			

			})
			.error(function(data, status, headers, config)
			{

			});				
		}			
		

	}
	
	

	
})

.controller('ManageApartmentsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicLoading,$timeout,$ionicModal,SendPostToServer) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.apartmentid = $stateParams.ItemId;
	$scope.buildingid = $stateParams.BuildingId;
	$scope.serverHost = $rootScope.serverHost;	
	
	$scope.dirotArray = [];
	$scope.selectedApparments = [];
	

			
	$scope.fields = 
	{
		"mifratdira" : "",
	}
	
	$scope.appfields = 
	{
		"name" : "",
		"hadarim" : "",
		"godeldira" : "",
		"godelmirpaset" : "",
		"mifratid" : "",
		"planid" : ""
	}
	
	$scope.saveOptions = function()
	{
		
		if ($scope.selectedApparments.length == 0)
		{
			$ionicPopup.alert({
				 title: 'יש לבחור דירות',
				 template: ''
			});	
		}
		else
		{
			
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: 'אפשריות שמירה',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [

			{
			text: 'ביטול',
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   
		   {
			text: 'הקם דירה',
			type: 'button-calm',
			onTap: function(e) { 
			 $scope.buildNewApp();
			}
		   },
		   
		   {
			text: 'בחר דירה',
			type: 'button-positive',
			onTap: function(e) { 
			  $scope.chooseApp();
			}
		   },
		  
		   ]
		  });			
		}

	}	
	
	

	
	$scope.getCustomApp = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $rootScope.selectedProjectNumber
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetCustomApparments',send_params)
		.success(function(data, status, headers, config)
		{
			console.log("custom app", data);
			$scope.ApptArray = data;

		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});					
	}
	
	
	$scope.getCustomApp();

	
	$scope.GetApartments = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

			
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"buildingid" : $scope.buildingid	
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetApartments',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			   
			   console.log("selected app:" , data);
			   $scope.ExistingAppData = data;

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});			
	}

	$scope.GetSingleBuilding = function()
	{
		$scope.GetApartments();	
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

			
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.buildingid
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetSingleBuilding',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();
			$scope.misparDirot = data[0].mispar_dirot;
			
			console.log("single building: ", data);
			
			
			$scope.GetApartments();
			
			$timeout(function() {
			for (i = 1; i <= $scope.misparDirot; i++)
			{
				   $scope.setSelected = "0";
				   for (g = 0; g < $scope.ExistingAppData.length; g++)
				   {
					   //alert ($scope.ExistingAppData[g].dira_mispar);
					   
						if ($scope.ExistingAppData[g].dira_mispar == i)
						{
							$scope.setSelected = "2";
						}
						else
						{
							//$scope.setSelected = "0";
						}	
				   }
				   
					

				   
				$scope.dirotArray.push({
					"index": i,
					"selected" : $scope.setSelected
				});	
			}		
			}, 500);		


		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});				
	}
	
	
	$scope.GetSingleBuilding();


	
	


	
	

	if ($scope.apartmentid == -1)
	{
		//$scope.navTitle = "הוספת דירה חדשה";
		$scope.Url = 'NewApartment';
		
	}
	else
	{
		//$scope.navTitle = "עריכת דירה";
		//$scope.GetSingleApartment();
		$scope.Url = 'EditApartment';

		
	}
	
	
	
	
	$scope.selectApp = function(index,id,selected)
	{
		if (selected == 2)
			return;
		
		if ($scope.dirotArray[index].selected == 0)
		{
			$scope.dirotArray[index].selected = 1;
			
			$scope.selectedApparments.push({
				"dira_mispar": id
			});	

			
		}
		else
		{
			$scope.dirotArray[index].selected = 0;
			
			for (i = 0; i < $scope.selectedApparments.length; i++)
			{

				if ($scope.selectedApparments[i].dira_mispar == id)
				{
					
					$scope.selectedApparments.splice(i, 1);
					//console.log (id+ " : " + $scope.selectedApparments[i].id+ " : "  + index);
				}
			}

			
		}
		console.log("selected dirot: ",$scope.selectedApparments)
	}	
	
	

	$scope.chooseApp = function()
	{
	   $ionicModal.fromTemplateUrl('templates/mifratdira-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(appModal) {
		  $scope.appModal = appModal;
		  $scope.appModal.show();
		});	
	}
	
	$scope.closeMifrat = function()
	{
		$scope.appModal.hide();
	}
	
	$scope.buildNewApp = function()
	{
		//alert ($rootScope.selectedProjectNumber);
		
		
	   $ionicModal.fromTemplateUrl('templates/newdira-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(newDiraModal) {
		  $scope.newDiraModal = newDiraModal;
		  $scope.newDiraModal.show();
		});	
	}
	
	$scope.closeAppModal = function()
	{
		$scope.newDiraModal.hide();
	}
	
	
	
	$scope.saveDira = function()
	{
		
		if ($scope.fields.mifratdira == "")
		{
			$ionicPopup.alert({
				 title: 'יש לבחור מפרט דירה',
				 template: ''
			});	
		}
		else
		{
			$scope.appModal.hide();
			$scope.saveApartment();			
		}

	}

	$scope.getMifratDira = function()
	{
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $rootScope.selectedProjectNumber,
		};

		HostUrl = $rootScope.LaravelHost+'/GetMifratimList'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.MifratimArray = data;
		});	

	}
	
	$scope.getMifratDira();


	$scope.getPlanList = function()
	{
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $rootScope.selectedProjectNumber,
		};

		HostUrl = $rootScope.LaravelHost+'/getPlanList'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.PlanListArray = data;
		});	

	}
	
	$scope.getPlanList();

	
	
	$scope.saveNewApp = function()
	{
		//validation 
		
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			//"id" : $scope.appId,
			"projectid" : $rootScope.selectedProjectNumber,
			"name" : $scope.appfields.name,
			"hadarim" : $scope.appfields.hadarim,
			"godeldira" : $scope.appfields.godeldira,
			"godelmirpaset" :$scope.appfields.godelmirpaset,
			"mifratid" : $scope.appfields.mifratid,
			"planid" : $scope.appfields.planid,
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/NewCustomApp',send_params)
		.success(function(data, status, headers, config)
		{

			
			$scope.fields.mifratdira = data[0].newid;
			
			
			//alert (data[0].newid)
			
			//$scope.fields.mifratdira = $scope.appfields.mifratid;
			
			
			$scope.closeAppModal();
			$scope.saveApartment();

		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});	
	}
	
	
	
	$scope.saveApartment = function()
	{
		//alert ($scope.fields.mifratdira);
		//return;
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

					
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $rootScope.selectedProjectNumber,
			"buildingid" : $scope.buildingid,
			"mifratdira" : $scope.fields.mifratdira,
			"apartments" : $scope.selectedApparments
		}
		//console.log(send_params)
		$http.post($rootScope.LaravelHost+'/AddNewApparment',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();

			window.history.back();


		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});	
	}
	
	

	
		
})
.controller('ApartmentsDetailsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,SendPostToServer) 
{
	$scope.ApartmentId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;	
	$scope.Comments = [];
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$rootScope.currentApartment = $stateParams.ItemId;
	
	$scope.contact = 
	{
		"name" : "",
		"phone" : "",
		"email" : "",
		"desc" : ""
	}
	
	$scope.GetSingleApartment = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"apartmentid" : $scope.ApartmentId	
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetSingleApartment',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("GetSingleApartment: " , data);
				$scope.projectDetails = data.project;
				$rootScope.projectImage = $scope.projectDetails.image;
				//alert ($scope.projectDetails.image);
				$scope.apartmentDetails = data.apartment[0];
				
				console.log("apartmentDetails: " , $scope.apartmentDetails)
				//alert ($scope.apartmentDetails.name)
				
				$rootScope.ApartmentsArray = $scope.apartmentDetails;

				$rootScope.currentProjectName = $scope.projectDetails.name;
				$rootScope.currentProjectAddress = $scope.projectDetails.address;			
				
				//alert ($scope.projectDetails.index);
				//alert ($scope.apartmentDetails.index);
				
				
				$scope.mifratimArray = data.mifratim;
				$scope.buildingDetails = data.building[0];
				//alert ($scope.projectDetails)
				$scope.Gallery = $scope.projectDetails.gallery;
				
				$scope.GalleryCount = $scope.Gallery.length;
				
				//alert ($scope.Gallery);
				if (data.comments)
				{
					$scope.Comments = data.comments;
					for(var i=0; i<data.comments.length; i++)
					{
						var DateStr = data.comments[i].time.split(" ");
						data.comments[i].time = DateStr[0]
					}
				}
				
				
				console.log("data.comments " , data.comments)

			})
			.error(function(data, status, headers, config)
			{

			});			
	}	
	
	$scope.GetSingleApartment();
	
	
	
	$scope.openGallery = function()
	{
		if ($scope.GalleryCount == 0)
		{
			$ionicPopup.alert({
				 title: 'לא נמצאו כרגע תמונות יש לנסות מאוחר יותר',
				 template: ''
			   });	
		}
		else
		{
		   $ionicModal.fromTemplateUrl('templates/gallery-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(galleryModal) {
			  $scope.galleryModal = galleryModal;
			  $scope.galleryModal.show();
			});				
		}
	
		
	}
	
	$scope.closeGallery = function()
	{
		$scope.galleryModal.hide();
	}
	
	$scope.contactPopup = function()
	{
		   $ionicModal.fromTemplateUrl('templates/contact-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(contactModal) {
			  $scope.contactModal = contactModal;
			  $scope.contactModal.show();
			});	
	}
	
	$scope.contactClose = function()
	{
		$scope.contactModal.hide();
	}
	
	
$scope.sendContact = function()
{
	
	if ($scope.contact.name =="")
	{
		$ionicPopup.alert({
		 title: 'יש למלא שם מלא',
		 template: ''
	   });			
	}
	else if ($scope.contact.phone =="")
	{
		$ionicPopup.alert({
		 title: 'יש למלא מספר טלפון',
		 template: ''
	   });			
	}
	else
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.projectDetails.index,
			"apartmentid" : $scope.apartmentDetails.index,
			"name":$scope.contact.name,
			"email":$scope.contact.email,
			"phone":$scope.contact.phone,
			"desc":$scope.contact.desc,
		};
		
		
		
		HostUrl = $rootScope.LaravelHost+'/sendContact'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();

			$ionicPopup.alert({
			 title: 'תודה פרטיך התקבלו בהצלחה, נחזור אליך בהקדם האפשרי',
			 template: ''
		   });	
		   
		   $scope.contactClose ();

		});		
	}
	
	

}	
	
	
	$scope.dialPhone = function()
	{

		window.location ="tel://"+$scope.projectDetails.phone;
	}
	
	$scope.Waze = function()
	{
		$scope.Waze = $scope.projectDetails.location_lat+','+$scope.projectDetails.location_lng;
		//alert ($scope.Waze)
		window.location = "http://waze.to/?ll="+$scope.Waze+"&navigate=yes";
	}
	
	$scope.openDiscussionModal = function()
	{
	   $ionicModal.fromTemplateUrl('details-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(discussionModal) {
		  $scope.discussionModal = discussionModal;
		  $scope.discussionModal.show();
		});
	}
	
    $scope.closeDiscussionModal = function() {
      $scope.discussionModal.hide();
    };
	
	
	$scope.openCommentModal = function()
	{
			  $scope.data = {};

			  // An elaborate, custom popup
				$ionicPopup.show({
				template: '<textarea placeholder="תוכן התגובה" style="direction:rtl;" rows="4" cols="50" ng-model="data.text" ></textarea>',
				
				title: 'הזן תוכן התגובה',
				//subTitle: 'Please use normal things',
				scope: $scope,
				buttons: [
				  { text: 'ביטול' },
				  {
					text: '<b>שליחה</b>',
					type: 'button-positive',
					onTap: function(e) {
					  if (!$scope.data.text) 
					  {
						//e.preventDefault();
					  } else 
					  {
						
						$ionicLoading.show({
						  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
						});	
				
						$scope.now = new Date(); 		
						$scope.month = $scope.now.getMonth()+1;
						$scope.month = ("0" + $scope.month).substr(-2);  
						$scope.day = ("0" + $scope.now.getDay()).substr(-2);
						$scope.then = $scope.now.getFullYear()+'-'+$scope.month+'-'+$scope.day; 
						$scope.hours = $scope.now.getHours();
						$scope.minutes = ("0" + $scope.now.getMinutes()).substr(-2);
						$scope.seconds  = ("0" + $scope.now.getSeconds()).substr(-2);
						$scope.newdate = $scope.then+ ' ' + $scope.hours+':'+ $scope.minutes+ ':' + $scope.seconds;
						

						
						
						
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

							send_params = 
							{
								"user" : $localStorage.userid,
								"apartmentid" : $scope.ApartmentId,
								"text" : $scope.data.text,
								"time" : $scope.newdate
							}
							//console.log(login_params)
							$http.post($rootScope.LaravelHost+'/AddApartmentComment',send_params)
							.success(function(data, status, headers, config)
							{
								
								$ionicLoading.hide();

								$scope.Comments.unshift({
									"image": $localStorage.image,
									"name": $localStorage.name,
									"text": $scope.data.text,
									"time" :  $scope.newdate
								});							

							})
							.error(function(data, status, headers, config)
							{
								$ionicLoading.hide();
							});							
					  }
					}
				  }
				]
			  });
	}


	$scope.showMifrat = function()
	{
	   $ionicModal.fromTemplateUrl('mifratim-modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(mifratModal) {
		  $scope.mifratModal = mifratModal;
		  $scope.mifratModal.show();
		});
	}
	
	$scope.closeMifratModal = function()
	{
		$scope.mifratModal.hide();
	}
	


	
	
})




.controller('AdminManageCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.logoutAdmin = function()
	{
		$localStorage.userid = '';
		window.location ="#/app/admin";
	}

	
})

.controller('CategoriesMifratCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.getCatagories = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

						
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetCatMifrat',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				console.log("Mifrat:",data);
				//$scope.MifratArray = data;
				$rootScope.MifratArray = data;
				
				

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
	}
	
	$scope.getCatagories();
	
	
	$scope.deleteCat = function(id,index)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteMifratCat',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.MifratArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}

})

.controller('ManageMifratCatCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera)
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.mifratId = $stateParams.ItemId;
	$scope.newimage = "";
	$scope.projectImage = $rootScope.projectImage;


	$scope.fields = 
	{
		"title" : "",
		"desc" : "",
		"serverimage" :  "",
		"image" : "" 
	}
	
	if ($scope.mifratId == -1)
	{
		$scope.Url = 'NewMifratCat';
		$scope.Id = "";
	}
	else
	{
		$scope.Url = 'EditMifratCat';
		
		if ($rootScope.MifratArray)
		{
			$scope.fields.title = $rootScope.MifratArray[$scope.mifratId].name;
			$scope.fields.desc = $rootScope.MifratArray[$scope.mifratId].desc;
			$scope.fields.image = $rootScope.MifratArray[$scope.mifratId].image;
			$scope.fields.serverimage = $rootScope.serverHost+$rootScope.MifratArray[$scope.mifratId].image;
			$scope.Id = $rootScope.MifratArray[$scope.mifratId].index;
		}
	}
	

	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	
	
	
	
	
	
	$scope.saveMifrat = function()
	{

		
		if ($scope.fields.title =="")
		{
			$ionicPopup.alert({
			 title: 'יש להזין כותרת',
			 template: ''
		   });				
		}
		else if ($scope.mifratId == -1 && $scope.newimage =="")
		{
			$ionicPopup.alert({
			 title: 'יש לבחור תמונה',
			 template: ''
		   });				
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		
			if ($scope.newimage)
				$scope.changedimage = $scope.newimage;
			else
				$scope.changedimage = $scope.fields.image;

			
			send_params = 
			{
				"id" : $scope.Id,
				"title" : $scope.fields.title,
				"desc" : $scope.fields.desc,
				"image" : $scope.changedimage
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				
				window.location ="#/app/categoriesmifrat";
				

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});			
		}
		
		
	
	}
	
	
	
	
	
	
})
.controller('MifratGalleryCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.mifratId = $stateParams.ItemId;
	$scope.host = $rootScope.serverHost;
	
	$scope.imagesArray = [];
	
	if ($rootScope.MifratArray[$scope.mifratId].gallery)
	{
		$scope.imagesArray = $rootScope.MifratArray[$scope.mifratId].gallery;
	}
	
	$scope.Id = $rootScope.MifratArray[$scope.mifratId].index;
	
	//alert ($scope.Id)
	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {


				send_params = 
				{
					"id" : $scope.Id,
					"image" : data.response
				}
				//console.log(login_params)
				$http.post($rootScope.LaravelHost+'/NewMifratGallery',send_params)
				.success(function(data, status, headers, config)
				{
					
				})
				.error(function(data, status, headers, config)
				{
					//$ionicLoading.hide();
				});		



					$scope.imagesArray.unshift({
						"image": data.response
					});	

		
				//alert (111)
				//alert (data.response)
				//$scope.newimage = data.response;
				//$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	
	

	
	console.log("Gallery", $rootScope.MifratArray);
})

.controller('BuildMifratCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';

	$scope.ProjectId = $stateParams.ProjectId;
	//$scope.ItemId = $stateParams.ItemId;
	$scope.Id = $stateParams.Id;
	
	
	//alert ($scope.ProjectId)
	//alert ($scope.ItemId)
	//alert ($scope.Id)
	
	$scope.mifratEdit = function(index)
	{
		//alert (index);
		window.location ="#/app/mifratmanage/"+$scope.ProjectId+"/"+$scope.Id+"/"+index;
	}


	
	
	$scope.getMifratim = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

						
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.ProjectId,
				"mifatlist" : $rootScope.selectedMifratList,
				"catid" : $scope.Id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetMifratim',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				//console.log("Mifrat:",data);
				$scope.MifratimArray = data;
				console.log("mifratim: " , $scope.MifratimArray)
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
	}
	
	$scope.getMifratim();
	
	
	$scope.selectMifratCat = function(id)
	{
		//alert (id)
		
		window.location ="#/app/selectmifrat/"+$scope.ProjectId+"/"+id;
	}
	
	
	
	$scope.deleteMifrat = function(index,id)
	{
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteMifrat',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.MifratimArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}

	
})

.controller('ManageMifratCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.mifratId = $stateParams.ItemId;
	$scope.projectId = $stateParams.ProjectNumber;
	//alert ($scope.projectId)
	
	$scope.selectedCatagories = [];
	
	$scope.fields = 
	{
		"title" : ""
	}

	$scope.getCatagories = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

						
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetCatMifrat',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.getSingleMifrat();
				//console.log("Mifrat:",data);
				$scope.MifratArray = data;
				for (i = 0; i < $scope.MifratArray.length; i++)
				{
					$scope.MifratArray[i].selected = 0;
				}				

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
	}
	
	$scope.getCatagories();


	$scope.getSingleMifrat = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

					
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.mifratId
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetSingleMifrat',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();
			//console.log("Mifrat:",data);
			$scope.MifratData = data[0];


			if ($scope.MifratData.catagories)
			$scope.selectedCatagories = $scope.MifratData.catagories;
				
			
			for (i = 0; i < $scope.MifratData.catagories.length; i++)
			{
				for (g = 0; g < $scope.MifratArray.length; g++)
				{
					if ($scope.MifratData.catagories[i].catagory_id == $scope.MifratArray[g].index)
					{
						$scope.MifratArray[g].selected = 1;
					}
				}
			}


			
			$scope.fields.title =  $scope.MifratData.name;
			
			

			
			console.log("single mifrat: " , $scope.MifratData)
		

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});				
	}
		
	
	
	if ($scope.mifratId == -1)
	{
		$scope.Url = 'AddNewMifrat';
	}
	else
	{
		$scope.Url = 'EditMifrat';
		//$scope.getSingleMifrat();
	}
	

	
	$scope.selectCatagory = function(index,id)
	{
		//alert (id);
		if ($scope.MifratArray[index].selected == 0)
		{
			$scope.MifratArray[index].selected = 1;
			
			
			$scope.selectedCatagories.push({
				"catagory_id": id
			});	

								
		}
		else
		{
			$scope.MifratArray[index].selected = 0;
			
			for (i = 0; i < $scope.selectedCatagories.length; i++)
			{

				if ($scope.selectedCatagories[i].catagory_id == id)
				{
					
					$scope.selectedCatagories.splice(i, 1);
					//console.log (id+ " : " + $scope.selectedCatagories[i].id+ " : "  + index);
				}
			}
				
		}
		
			//console.log("selectedCatagories22: ",$scope.selectedCatagories)
	}	
	
	
	$scope.saveMifrat = function()
	{

		if ($scope.fields.title == "")
		{
			$ionicPopup.alert({
				 title: 'יש להזין כותרת',
				 template: ''
			 });				
		}
		
		else if ($scope.selectedCatagories.length == 0)
		{
			$ionicPopup.alert({
				 title: 'יש תחילה לבחור קטגוריות',
				 template: ''
			 });				
		}
		else
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


				send_params = 
				{
					"user" : $localStorage.userid,
					"id" : $scope.mifratId,
					"projectid" : $scope.projectId,
					"title" : $scope.fields.title,
					"selectedcat" : $scope.selectedCatagories
					//"id" : $scope.Id,
				}
				//console.log(login_params)
				$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
				.success(function(data, status, headers, config)
				{
					window.history.back();
					//window.location ="#/app/buildmifrat";
					

				})
				.error(function(data, status, headers, config)
				{
					//$ionicLoading.hide();
				});				
		}
		//alert ($scope.selectedCatagories);
		console.log ("selectedCatagories: ",$scope.selectedCatagories);
	}
	


	

})

.controller('CustomAparmentCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.projectId = $stateParams.ItemId;
	
	//alert($scope.projectId)
	
	
	$scope.getCustomApp = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.projectId
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetCustomApp',send_params)
		.success(function(data, status, headers, config)
		{
			
			$scope.CustomApptArray = data;

		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});					
	}
	
	
	$scope.getCustomApp();
	
	
	
	$scope.deleteApp = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteCustomApp',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.CustomApptArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}


})


.controller('ManageAparmentCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	$scope.ProjectId = $stateParams.Project;
	$scope.appId = $stateParams.ItemId;
	
	//alert ($scope.ProjectId)
	
	
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	$scope.fields = 
	{
		"name" : "",
		"hadarim" : "",
		"godeldira" : "",
		"godelmirpaset" : "",
		"mifratid" : "",
		"planid"  : ""
	}
	
	
	
	$scope.getMifratim = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

						
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.ProjectId
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetMifratimList',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				//console.log("Mifrat:",data);
				$scope.MifratimArray = data;
				console.log("mifratim: " , $scope.MifratimArray)
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
	}
	
	$scope.getMifratim();
	
	
	$scope.getPlanList = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.ProjectId,
		};

		
		HostUrl = $rootScope.LaravelHost+'/getPlanList'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.PlanListArray = data;
		   
		});
	}
	
	$scope.getPlanList();


	
	$scope.getSingleApp = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.appId
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetSingleCustomApp',send_params)
		.success(function(data, status, headers, config)
		{

			 $timeout(function() {
				$scope.AppArray = data[0];
				$scope.fields.name = $scope.AppArray.name;
				$scope.fields.hadarim = $scope.AppArray.mispar_hadarim;
				$scope.fields.godeldira = $scope.AppArray.godel_dira;
				$scope.fields.godelmirpaset = $scope.AppArray.godel_mirpaset;
				$scope.fields.mifratid = $scope.AppArray.mifrat_id;
				$scope.fields.planid = $scope.AppArray.plan_id;
			}, 300);

					


		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});					
	}
	


	
	if ($scope.appId == -1)
	{
		$scope.Url = 'NewCustomApp';
	}
	else
	{
		$scope.Url = 'EditCustomApp';
		$scope.getSingleApp();
		
	}
	
	
	$scope.saveApartment = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.appId,
			"projectid" : $scope.ProjectId,
			"name" : $scope.fields.name,
			"hadarim" : $scope.fields.hadarim,
			"godeldira" : $scope.fields.godeldira,
			"godelmirpaset" :$scope.fields.godelmirpaset,
			"mifratid" : $scope.fields.mifratid,
			"planid" : $scope.fields.planid,
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
		.success(function(data, status, headers, config)
		{
			
			window.location ="#/app/customaparment/"+$scope.ProjectId;

		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});			
	}

})

.controller('ManageBuildingsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.buildingId = $stateParams.ItemId;
	$scope.projectId = $stateParams.ProjectId;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.projectImage = $rootScope.projectImage;
	

	$scope.fields = 
	{
		"name" : "",
		"mispardirot" : "",
		"desc" : "",
		"image" : "",
		"serverimage" : ""
	}
	
	

	$scope.GetSingleBuilding = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

					
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.buildingId
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetSingleBuilding',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();

			
			$scope.buildingData = data[0];
			
			
			$scope.fields.name = $scope.buildingData.name;
			$scope.fields.mispardirot = $scope.buildingData.mispar_dirot;
			$scope.fields.desc = $scope.buildingData.desc;
			$scope.fields.image = $scope.buildingData.image;
			$scope.fields.serverimage = $rootScope.serverHost+$scope.buildingData.image;
			


		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});				
	}

	
	
	if ($scope.buildingId == -1)
	{
		$scope.Url = 'NewBuilding';
	}
	else
	{
		$scope.Url = 'EditBuilding';
		$scope.GetSingleBuilding();
		
	}
	
	

	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}

	$scope.openOptions = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'האם ברצונך ליצור עוד בניין?',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'כן',
		type: 'button-positive',
		onTap: function(e) { 
		  
		}
	   },
	   {
		text: 'לא',
		type: 'button-calm',
		onTap: function(e) { 
		 window.location = "#/app/projects";
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}


	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {


				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	


	
	
	$scope.saveBuilding = function()
	{
		
		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם הבניין',
				 template: ''
			});				
		}
		else if ($scope.fields.mispardirot =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין מספר דירות',
				 template: ''
			});				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			if ($scope.newimage)
				$scope.customImage = $scope.newimage
			else
				$scope.customImage = $scope.fields.image;

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.buildingId,
				"projectid" : $scope.projectId,
				
				"name" : $scope.fields.name,
				"mispardirot" : $scope.fields.mispardirot,
				"desc" : $scope.fields.desc,
				"image" : $scope.customImage
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				$scope.fields.name = '';
				$scope.fields.mispardirot = '';
				$scope.fields.desc = '';
				$scope.newimage = '';
				
				if ($scope.buildingId == -1)
					$scope.openOptions();
				else
					window.history.back();
				
				

			})
			.error(function(data, status, headers, config)
			{
				//$ionicLoading.hide();
			});				
		}
	
		
		
		
		
		
	}
	
	
})

.controller('EditApartmentCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.appId = $stateParams.ItemId;
	$scope.ApartmentData = $rootScope.ApartmentsArray;
	$scope.AppIndex = $scope.ApartmentData.index;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	//alert ($scope.AppIndex);
	
	
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"koma" : "",
		"mifratid" : ""
	}

	$scope.setData = function()
	{
		 $timeout(function() {
			$scope.fields.name = $scope.ApartmentData.name;
			$scope.fields.phone = $scope.ApartmentData.phone;
			$scope.fields.mail = $scope.ApartmentData.mail;
			$scope.fields.koma = $scope.ApartmentData.floornumber;
			$scope.fields.mifratid = $scope.ApartmentData.mifrat_id;
		}, 300);
			
	}
	
	$scope.setData();
	


	$scope.getCustomApp = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $rootScope.selectedProjectNumber
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetCustomApp',send_params)
		.success(function(data, status, headers, config)
		{
			console.log("MifratimArray", data);
			$scope.MifratimArray = data;


			 $timeout(function() {
				$scope.fields.name = $scope.ApartmentData.name;
				$scope.fields.phone = $scope.ApartmentData.phone;
				$scope.fields.mail = $scope.ApartmentData.mail;
				$scope.fields.koma = $scope.ApartmentData.floornumber;
				$scope.fields.mifratid = $scope.ApartmentData.mifrat_id;
			}, 300);

		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});					
	}
	
	
	//$scope.getCustomApp();

	
	
	$scope.saveApp = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.AppIndex,
			"name" : $scope.fields.name,
			"phone" : $scope.fields.phone,
			"mail" : $scope.fields.mail,
			"koma" : $scope.fields.koma,
			"mifratid" : $scope.fields.mifratid
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/SaveApartment',send_params)
		.success(function(data, status, headers, config)
		{
			
			
			
			window.location ="#/app/apartmentdetails/"+$scope.AppIndex;
			
			
			//window.history.back();

		})
		.error(function(data, status, headers, config)
		{
			//$ionicLoading.hide();
		});	
	}

})	

.controller('AparmentUsersCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,$cordovaContacts) 
{
	$scope.appId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"email" : ""
	}
	
	$scope.getUsers = function()
	{
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.appId
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetAppUsers',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			
			$scope.UsersArray = data;
			$rootScope.AppUsersArray = data;
			
			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});	
	}
	
	
	$scope.getUsers();
	
	
	$scope.getName = function(c) {
		var name = c.displayName;
		if(!name || name === "") {
			if(c.name.formatted) return c.name.formatted;
			if(c.name.givenName && c.name.familyName) return c.name.givenName +" "+c.name.familyName;
			return "";
		}
		return name;
	}

	
	
	 $scope.pickContactUsingNativeUI = function () {
		$cordovaContacts.pickContact().then(function (contactPicked) {
		  $scope.contact = contactPicked;
		  //console.log ($scope.contact.phoneNumbers[0].value)
		  
		  $scope.selectedPhones = $scope.contact.phoneNumbers[0].value;

		  $scope.fields.name = $scope.getName($scope.contact);
		  $scope.fields.phone = $scope.selectedPhones;

		  
        if($scope.contact.emails && $scope.contact.emails.length) {
			$scope.fields.email = $scope.contact.emails[0].value;
        }
		
			if ($scope.selectedPhones)
			{
				$ionicLoading.show({
				  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
				});	

							
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

				send_params = 
				{
					"user" : $localStorage.userid,
					"apparmentid" : $scope.appId,
			
					"name" : $scope.fields.name, 
					"phone" : $scope.fields.phone, 
					"mail" : $scope.fields.email, 
			
				}
				//console.log(login_params)
				$http.post($rootScope.LaravelHost+'/AddAppUser',send_params)
				.success(function(data, status, headers, config)
				{
					
					$ionicLoading.hide();
					$scope.getUsers();
					//window.history.back();

				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});						
			}
		
	

		
		});
	  }	

	  
	
	$scope.newUserOptions = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'יש לבחור',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'הוספה ידנית',
		type: 'button-positive',
		onTap: function(e) { 
		  window.location = "#/app/manageappusers/"+$scope.appId+"/-1";
		}
	   },
	   {
		text: 'הוספה מאנשי קשר',
		type: 'button-calm',
		onTap: function(e) { 
		  $scope.pickContactUsingNativeUI();
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}


	

	
	$scope.deleteUser = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteAppUser',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.UsersArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}


})	

.controller('ManageAppusersCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.ApparmentId = $stateParams.ApparmentId;
	$scope.ItemId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : ""
	}
	
	

	if ($scope.ItemId == -1)
	{
		$scope.navTitle = 'הוספת משתמש חדש';
		$scope.URL = 'AddAppUser';
		$scope.userId = '0';
	}
	else
	{
		$scope.navTitle = 'עריכת משתמש';
		$scope.URL = 'SaveAppUser';
		
		
		$scope.userData = $rootScope.AppUsersArray[$scope.ItemId];
		$scope.userId = $scope.userData.index;
		
		
		$timeout(function() {
			 
		$scope.fields.name = $scope.userData.name;
		$scope.fields.phone = $scope.userData.phone;
		$scope.fields.mail	= $scope.userData.email; 
			 

		}, 300);

	}
	

	$scope.saveUser = function()
	{
		
		if ($scope.fields.name == "")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם מלא',
				 template: ''
			});				
		}
		else if ($scope.fields.phone == "")
		{
			$ionicPopup.alert({
				 title: 'יש להזין מספר טלפון',
				 template: ''
			});				
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"apparmentid" : $scope.ApparmentId,
				"id" : $scope.userId,
				
				"name" : $scope.fields.name, 
				"phone" : $scope.fields.phone, 
				"mail" : $scope.fields.mail, 
		
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.URL,send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				window.history.back();

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});				
		}
		
		
		
	}
	
	
	
	
})

.controller('PlanCatCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	
	$scope.ItemId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.projectImage = $rootScope.projectImage;


	
	$scope.getPlanCategories  = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

			
		params = 
		{
			"user" : $localStorage.userid,
			"apartmentid" : $scope.ItemId
		};
		
		HostUrl = $rootScope.LaravelHost+'/getPlanCat'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.planCategories = data;
		});
	}
	
	$scope.getPlanCategories();
	
	
	$scope.goPlan = function(item)
	{
		//alert (item.plan_list);
		//alert (item.category)
		window.location ="#/app/apartmentplans/"+item.plan_list+"/"+item.category;

	}
	
	
})

.controller('ApartmentPlansCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	$scope.PlanList = $stateParams.PlanList;
	$scope.Category = $stateParams.Category;
	$scope.projectImage = $rootScope.projectImage;	
	
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.projectName = $rootScope.currentProjectName;
	$scope.projectAddress = $rootScope.currentProjectAddress;

	//alert ($rootScope.selectedProjectNumber);
	
	$scope.GetAppBuild = function()
	{
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

			
		console.log("Data : ") ;
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $rootScope.selectedProjectNumber,
			"planlist" : $scope.PlanList,
			"category" : $scope.Category,
		};
		
		HostUrl = $rootScope.LaravelHost+'/GetAppBuild'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			console.log("data", data);
			$scope.AppBuild = data;
			$rootScope.BuildDataArray = data;
		});
	}
	
	$scope.GetAppBuild();

	
	$scope.goBuildDetails = function(index)
	{
		window.location ="#/app/builddetails/"+index;
	}
})	


.controller('MifratCatCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.ApparmentId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	//$scope.projectDetails = $rootScope.currentProject;
	$scope.projectName = $rootScope.currentProjectName;
	$scope.projectAddress = $rootScope.currentProjectAddress;
	$scope.projectImage = $rootScope.projectImage;


	$scope.getMifratCat = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

					
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"apartmentid" : $scope.ApparmentId,
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetAppCat',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();
			$scope.mifratCategories = data;
			console.log("mifrat data:" , data);
			//alert (data);
			//alert ($scope.mifratimArray);
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
	}
	
	$scope.getMifratCat();
	
	
	$scope.goMifrat = function(item)
	{	
		window.location ="#/app/apartmentmifrat/"+item.mifrat_list+"/"+item.category;
	}
	
	
	$scope.goGallery = function(id,count)
	{
		if (count > 0)
		window.location ="#/app/categorygallery/"+id;
	}
	
	
})	


.controller('ApartmentMifratCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	$scope.MifratList = $stateParams.MifratList;
	$scope.Category = $stateParams.Category;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.projectName = $rootScope.currentProjectName;
	$scope.projectAddress = $rootScope.currentProjectAddress;
	$scope.projectImage = $rootScope.projectImage;

	
	$scope.GetAppMifrat = function()
	{
		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

			
		console.log("Data : ") ;
		params = 
		{
			"user" : $localStorage.userid,
			"mifratlist" : $scope.MifratList,
			"category" : $scope.Category,
		};
		
		HostUrl = $rootScope.LaravelHost+'/GetAppMifrat'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			console.log("data", data);
			$scope.AppMifratim = data;
			$rootScope.MifratDataArray = data;
		});
	}
	
	$scope.GetAppMifrat();

	
	$scope.goMifratDetails = function(index)
	{
		window.location ="#/app/mifratdetails/"+index;
	}
	
	
	
})


.controller('MifratDetailsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.ItemId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';	
	$scope.projectName = $rootScope.currentProjectName;
	$scope.projectAddress = $rootScope.currentProjectAddress;

	
	$scope.mifratDetails = $rootScope.MifratDataArray[$scope.ItemId];
	
	
})	


.controller('BuildDetailsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.ItemId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';	
	$scope.projectName = $rootScope.currentProjectName;
	$scope.projectAddress = $rootScope.currentProjectAddress;

	
	$scope.buildDetails = $rootScope.BuildDataArray[$scope.ItemId];
	
	
})	

.controller('CategoryGalleryCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera) 
{
	$scope.CategoryId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	$scope.getCatGallery = function()
	{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"catid" : $scope.CategoryId,
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetCatGallery',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.mifratGallery = data;

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});			
	}
	
	$scope.getCatGallery();	

})	


.controller('chatPageCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.mifratId = $stateParams.ItemId;
	$scope.activeTab = 1;
	$scope.projects = [];
	$scope.serverhost = $rootScope.serverHost;
	
	$scope.setactiveTab = function(num)
	{
		$scope.activeTab = num;
	}
	
	$scope.getProjects = function()
	{
		console.log("Data : ") ;
		params = 
		{
			"ApartmentId":$rootScope.currentApartment
		};
		
		HostUrl = $rootScope.LaravelHost+'/GetChatProjects'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			console.log("Data : " , data) ;
			$scope.projects = data;
			$rootScope.chatProjects = data;
		});
	}
	
	$scope.deleteProject = function(index)
	{
		console.log("IDDD " , index)
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) 
	   {
		    params = 
			{
				"ProjectId":index
			};
			
			console.log("Delete : " , params)
			HostUrl = $rootScope.LaravelHost+'/DeleteChatProject'	
				
			SendPostToServer(params,HostUrl,function(data, success) 
			{
				$scope.getProjects();
			});
	   })
	   
	   
	}
	
	
	$scope.getProjects();
})


.controller('chatProjectCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.newimage = "";
	$scope.fields = 
	{
		"category":"",
		"place":"",
		"error":"",
		"currentApartment":$rootScope.currentApartment,
		"user" : $localStorage.userid,
		"image": $scope.newimage
	}
	

	
	
	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) 
				{
					$scope.newimage = data.response;
					$scope.fields.serverimage = $rootScope.serverHost+data.response;
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	

	
	params = {};
	HostUrl = $rootScope.LaravelHost+'/GetChatCategory'	
		
	SendPostToServer(params,HostUrl,function(data, success) 
	{
		$scope.Categories = data;
	});
	
	$scope.addProject = function()
	{
		if ($scope.fields.category =="")
		{
			$ionicPopup.alert({
				 title: 'עליך לבחור קטגורייה',
				 template: ''
			});	
		}
		else if ($scope.fields.place =="")
		{
			$ionicPopup.alert({
				 title: 'הזן מיקום בבית',
				 template: ''
			});	
		}else if ($scope.fields.error =="")
		{
			$ionicPopup.alert({
				 title: 'הזן הערה',
				 template: ''
			});	
		} else
		{
			
			params = 
			{
				"user" : $localStorage.userid,
				"category" : $scope.fields.category,
				"place" : $scope.fields.place,
				"error" : $scope.fields.error,
				"currentApartment" : $scope.fields.currentApartment,
				"image" : $scope.newimage,
			};



			HostUrl = $rootScope.LaravelHost+'/InsertChatProject'	
			
			SendPostToServer(params,HostUrl,function(data, success) 
			{
				console.log("fields : " , $scope.fields)
				window.location ="#/app/chatPage/"+$rootScope.currentApartment;
			});
		}
	}
	

})


.controller('EditChatProjectCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{																		 
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ItemId;
	console.log("ProjectId : " , $stateParams.ItemId)
	
	for(i=0;i<$rootScope.chatProjects.length;i++)
	{
		console.log("ProjectId1 : " , $stateParams.ItemId)
		if($rootScope.chatProjects[i].index == $scope.ProjectId)
		$scope.Project = $rootScope.chatProjects[i]
	}


	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) 
				{
					$scope.newimage = data.response;
					$scope.fields.serverimage = $rootScope.serverHost+data.response;
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	


	

	
	params = {};
	HostUrl = $rootScope.LaravelHost+'/GetChatCategory'	
		
	SendPostToServer(params,HostUrl,function(data, success) 
	{
		$scope.Categories = data;
	});
	

	
	$scope.fields = 
	{
		"category":$scope.Project.category,
		"place":$scope.Project.place,
		"error":$scope.Project.error,
		"currentApartment":$rootScope.currentApartment,
		"user" : $localStorage.userid,
		"projectId" : $scope.Project.index,
		"image" : $scope.uploadimage,
		"serverimage" : ""
	}
	

	
	
	if ($scope.Project.image)
	{
		$scope.fields.serverimage = $rootScope.serverHost+$scope.Project.image;
	}	
	
	
	console.log("Project : " , $scope.Project)
	
	$scope.UpdateProject = function()
	{
		if ($scope.fields.category =="")
		{
			$ionicPopup.alert({
				 title: 'עליך לבחור קטגורייה',
				 template: ''
			});	
		}
		else if ($scope.fields.place =="")
		{
			$ionicPopup.alert({
				 title: 'הזן מיקום בבית',
				 template: ''
			});	
		}else if ($scope.fields.error =="")
		{
			$ionicPopup.alert({
				 title: 'הזן הערה',
				 template: ''
			});	
		} else
		{
			
			if ($scope.newimage)
				$scope.uploadimage = $scope.newimage;
			else
				$scope.uploadimage = $scope.Project.image;

			
			//alert ($scope.uploadimage);
			
			
			params = 
			{
				"category": $scope.fields.category,
				"place": $scope.fields.place,
				"error": $scope.fields.error,
				"currentApartment": $scope.fields.currentApartment,
				"user" : $localStorage.userid,
				"projectId" : $scope.fields.projectId,
				"image" : $scope.uploadimage,
			};
	
	
	
			console.log("UpdateFields : " ,$scope.fields )
			HostUrl = $rootScope.LaravelHost+'/UpdateChatProject'	
			
			SendPostToServer(params ,HostUrl,function(data, success) 
			{
				console.log("fields : " , $scope.fields)
				window.location ="#/app/chatPage/"+$rootScope.currentApartment;
			});
		}
	}
	
})


.controller('chatDetailsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ItemId;
	$scope.serverHost = $rootScope.serverHost;
	
	$scope.PopupReview = 
	{
		"title":""
	};
	$scope.Reviews = [];
	
	var ChatPopup = "";	
	
	for(i=0;i<$rootScope.chatProjects.length;i++)
	{
		console.log("ProjectId1 : " , $stateParams.ItemId)
		if($rootScope.chatProjects[i].index == $scope.ProjectId)
		$scope.Project = $rootScope.chatProjects[i]
	}
	
	
	$scope.getReviews = function()
	{
		$scope.ProjectInfo = 
		{
			"ProjectId":$scope.ProjectId
		}
		
		HostUrl = $rootScope.LaravelHost+'/getReviews'	
		
		SendPostToServer($scope.ProjectInfo,HostUrl,function(data, success) 
		{
			console.log("Reviews : " , data)
			$scope.Reviews = data;
		});
	}
	
	$scope.getReviews();	
	
	
	$scope.fields = 
	{
		"category":$scope.Project.category,
		"place":$scope.Project.place,
		"error":$scope.Project.error,
		"currentApartment":$rootScope.currentApartment,
		"user" : $localStorage.userid,
		"projectId" : $scope.Project.index,
		"serverimage" : ""
	}
	
	if ($scope.Project.image)
		$scope.fields.serverimage = $rootScope.serverHost+$scope.Project.image;
	else
		$scope.fields.serverimage = "";

	$scope.openPopUp = function()
	{
		ChatPopup = $ionicPopup.show({
			templateUrl: 'templates/chatPopUp.html',
			scope: $scope,
			cssClass: 'ChatPopup'
		});
	}
	
	$scope.HideChatPopup = function () 
	{
		if($scope.PopupReview.title != "")
		{
			console.log("s1")
			$scope.fields = 
			{
				"projectid":$scope.ProjectId,
				"userid":$localStorage.userid,
				"title":$scope.PopupReview.title
			}
			
			HostUrl = $rootScope.LaravelHost+'/insertReviews'	
			
			SendPostToServer($scope.fields,HostUrl,function(data, success) 
			{
				console.log("s2")
				$scope.PopupReview.title = ""
				ChatPopup.close();
				$scope.getReviews();
			});
		}
		else
		{	
			console.log("s3")
			ChatPopup.close();
		}
		
		
		
	};	
	
})


.controller('AboutCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce,$ionicSlideBoxDelegate) 
{

	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.serverHost = $rootScope.serverHost;

	$scope.getStatus = function(type)
	{
		var projectStatus = "";
		
		if (type == 1)
			projectStatus = "בתכנון";

		if (type == 2)
			projectStatus = "בביצוע";

		if (type == 3)
			projectStatus = "סיום בנייה";

		return projectStatus;
	}



			
	
	$scope.getAbout = function()
	{
		
		params = {};	
		
		$scope.HostUrl = $rootScope.LaravelHost+'/GetAbout'	
		
	
		SendPostToServer(params,$scope.HostUrl,function(data, success) 
		{
			$scope.AboutGallery = data.gallery;
			$ionicSlideBoxDelegate.update();
			console.log("about", data);
			$scope.AboutData = data[0];
			$scope.desc = $sce.trustAsHtml($scope.AboutData.desc);
		});
	}
	
	$scope.getAbout();
	
	$scope.getProjects = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetProjects',send_params)
			.success(function(data, status, headers, config)
			{
				console.log("projects: " , data);
				$scope.ProjectsArray = data;
				$rootScope.ProjectsJson = data;
				
			})
			.error(function(data, status, headers, config)
			{

			});	

			
	}
	
	$scope.getProjects();


	
	
})


.controller('ProjectDetailsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.serverHost = $rootScope.serverHost;
	$scope.ItemId = $stateParams.ItemId;
	$scope.projectData = $rootScope.ProjectsJson[$scope.ItemId];
	//alert ($scope.projectData.index);

	$scope.contact = 
	{
		"name" : "",
		"phone" : "",
		"email" : "",
		"desc" : ""
	}

	//alert ($rootScope.currentApartment);
	
	console.log("project: ", $scope.projectData)
	
	$scope.projectGallery = $scope.projectData.gallery;


				
	//alert ($scope.projectData.location_lat)
	//alert ($scope.projectData.location_lng)

	$scope.openWaze = function()
	{
		if ($scope.projectData.location_lat)
		{
			$scope.Waze = $scope.projectData.location_lat+","+$scope.projectData.location_lng
			window.location = "http://waze.to/?ll="+$scope.Waze+"&navigate=yes";
		}
	}
	
	$scope.contactPopup = function()
	{
		   $ionicModal.fromTemplateUrl('templates/contact-modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(contactModal) {
			  $scope.contactModal = contactModal;
			  $scope.contactModal.show();
			});	
	}
	
	$scope.contactClose = function()
	{
		$scope.contactModal.hide();
	}
	
	
$scope.sendContact = function()
{
	
	if ($scope.contact.name =="")
	{
		$ionicPopup.alert({
		 title: 'יש למלא שם מלא',
		 template: ''
	   });			
	}
	else if ($scope.contact.phone =="")
	{
		$ionicPopup.alert({
		 title: 'יש למלא מספר טלפון',
		 template: ''
	   });			
	}
	else
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.projectData.index,
			"apartmentid" : $rootScope.currentApartment,
			"name":$scope.contact.name,
			"email":$scope.contact.email,
			"phone":$scope.contact.phone,
			"desc":$scope.contact.desc,
		};
		
		
		
		HostUrl = $rootScope.LaravelHost+'/sendContact'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();

			$ionicPopup.alert({
			 title: 'תודה פרטיך התקבלו בהצלחה, נחזור אליך בהקדם האפשרי',
			 template: ''
		   });	
		   
		   $scope.contactClose ();

		});		
	}
	
}

	
})


.controller('SelectMifratCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{

	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.serverHost = $rootScope.serverHost;
	
	$scope.ProjectId = $stateParams.ProjectId;
	$scope.ItemId = $stateParams.ItemId;
	
	//alert ($scope.ProjectId)
	//alert ($scope.ItemId)
	
	$scope.pickMifrat = function(id)
	{
		//alert (id);
		window.location ="#/app/mifratcategories/"+$scope.ProjectId+"/"+id;
	}
	
	$scope.getCatagories = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

						
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.ProjectId,
				"listid" : $rootScope.selectedMifratList
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/GetCatMifrat',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				console.log("Mifrat:",data);
				$scope.MifratArray = data;
				

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
	}
	
	$scope.getCatagories();

})	


.controller('MifratCategoriesCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ProjectId;
	//$scope.ItemId = $stateParams.ItemId;
	$scope.Id = $stateParams.Id;
	
	//alert ($scope.ProjectId)
	//alert ($scope.ItemId)
	//alert ($scope.Id)
	
	$scope.mifratEdit = function(index)
	{
		//alert (index);
		
		window.location ="#/app/mifratmanage/"+$scope.ProjectId+"/"+$scope.Id+"/"+index;
		
		
	}

	
	
})	

.controller('MifratManageCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	$scope.ProjectId = $stateParams.ProjectId;
	$scope.ItemId = $stateParams.ItemId;
	$scope.Id = $stateParams.Id;
	
	//alert ($scope.ProjectId)
	//alert ($scope.ItemId)
	
	$scope.fields = 
	{
		"name" : "",
		"supplier" : "",
		"coupon" : "",
		"quan" : "",
		"desc" : "",
		"serverimage" : "",
		"image" : ""
	}
	
	//alert ($scope.ProjectId)
	//alert ($scope.ItemId)
	

	

	
	$scope.getSingleMifrat = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

					
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"id" : $scope.Id
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetSingleMifrat',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();
			//console.log("Mifrat:",data);
			$scope.MifratData = data[0];
			
			$scope.fields.name = $scope.MifratData.name;
			$scope.fields.supplier = $scope.MifratData.supplier;
			$scope.fields.coupon = $scope.MifratData.company_promo;
			$scope.fields.quan = $scope.MifratData.quan;
			$scope.fields.desc = $scope.MifratData.desc;
			$scope.fields.image = $scope.MifratData.image;
			
			if ($scope.MifratData.image)
				$scope.fields.serverimage = $rootScope.serverHost+$scope.MifratData.image;
			

			console.log("single mifrat: " , $scope.MifratData)

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});				
	}	

	if ($scope.Id == -1)
	{
	  $scope.Url = "AddNewMifrat";
	}
	else
	{
		$scope.Url = "EditMifrat";
		$scope.getSingleMifrat();
	}

	
	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			
		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }		
	
	
	
	$scope.saveMifrat = function()
	{
		
		if ($scope.newimage)
			$scope.uploadimage = $scope.newimage;
		else
			$scope.uploadimage = $scope.fields.image;
		
		
		
		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם המפרט',
				 template: ''
			});		
		}	
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.Id,
				"project" : $scope.ProjectId,
				"category" :  $scope.ItemId,
				"mifratlist" : $rootScope.selectedMifratList, 
				
				"name" :  $scope.fields.name,
				"supplier" :  $scope.fields.supplier,
				"coupon" :  $scope.fields.coupon,
				"quan" :  $scope.fields.quan,
				"desc" :  $scope.fields.desc,
				"image" :  $scope.uploadimage,
			}

			
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				

				window.location ="#/app/mifratcategories/"+$scope.ProjectId+"/"+$scope.ItemId;


			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});		
}




	
	}
	
})	


.controller('ProjectSettings', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectNumber = $stateParams.ProjectId;
	
	//alert ($scope.ProjectId)
	
})	

.controller('MifratListCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectNumber = $stateParams.ProjectId;

	$scope.chooseMifrat = function(id)
	{
		//alert (id);
		$rootScope.selectedMifratList = id;
		//alert (id)
		window.location ="#/app/selectmifrat/"+$scope.ProjectNumber;
	}
	

	
	
	$scope.getMifratimList = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

						
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.ProjectNumber,
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetMifratimList',send_params)
		.success(function(data, status, headers, config)
		{
			
			$ionicLoading.hide();
			//console.log("Mifrat:",data);
			$scope.MifratimListArray = data;
			$rootScope.MifratListArray = $scope.MifratimListArray;
			
			console.log("mifratim: " , $scope.MifratimListArray)
		

		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});			
	}
	
	$scope.getMifratimList();
	


	$scope.deleteList = function(id,index)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteList',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.MifratimListArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}

	
})


.controller('ManageMifratList', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectNumber = $stateParams.ProjectId;
	$scope.MifratId = $stateParams.Id;	
	$scope.Id = "";
	//alert ($scope.MifratId)
	
	$scope.fields = 
	{
		"name" : ""
	}
	
	if ($scope.MifratId == -1)
	{
		$scope.Url = "AddMifratList";
	}
	else
	{
		$scope.Url = "EditMifratList";
		
		if ($rootScope.MifratListArray)
		{
			$scope.MifratData = $rootScope.MifratListArray[$scope.MifratId];
			$scope.fields.name = $scope.MifratData.name;
			$scope.Id = $scope.MifratData.index;
		}
		
		
	}
	
	//alert ($scope.Url)
	
	$scope.saveList = function()
	{

		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם המפרט',
				 template: ''
			});		
		}	

		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

					
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.Id,
				"projectid" : $scope.ProjectNumber,
				"name" : $scope.fields.name
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();

				
				window.location ="#/app/mifratlist/"+$scope.ProjectNumber;
					
				
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});				
		}
		
	}
	
	



})

.controller('CategoriesPlansCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	
	$scope.getPlans = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

				
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

		send_params = 
		{
			"user" : $localStorage.userid,
		}
		//console.log(login_params)
		$http.post($rootScope.LaravelHost+'/GetCatPlans',send_params)
		.success(function(data, status, headers, config)
		{
			$ionicLoading.hide();
			$scope.PlansJson = data;
			$rootScope.PlansCatArray = data;			
		})
		.error(function(data, status, headers, config)
		{
			$ionicLoading.hide();
		});				
	}
	
	$scope.getPlans();
	
	
	$scope.deletePlan = function(id,index)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

	
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteCatPlan',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.PlansJson.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}

})

.controller('ManagePlanCatCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.itemId = $stateParams.ItemId;
	$scope.Id = "";
	$scope.newimage = "";
	
	
	$scope.fields = 
	{
		"title" : "",
		"desc" : "",
		"image" : "",
		"serverimage" : ""
	}

	
	//alert ($scope.itemId)
	
	if ($scope.itemId == -1)
	{
		$scope.Url = "NewPlanCat";
	}	
	else
	{		
		$scope.Url = 'EditPlanCat';
		
		if ($rootScope.PlansCatArray)
		{
			$scope.fields.title = $rootScope.PlansCatArray[$scope.itemId].name;
			$scope.fields.desc = $rootScope.PlansCatArray[$scope.itemId].desc;
			$scope.fields.image = $rootScope.PlansCatArray[$scope.itemId].image;
			$scope.fields.serverimage = $rootScope.serverHost+$rootScope.PlansCatArray[$scope.itemId].image;
			$scope.Id = $rootScope.PlansCatArray[$scope.itemId].index;
		
		}

		
	}
	
	
	

	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			
		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	
	
	
	$scope.savePlan = function()
	{

		if ($scope.fields.title =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין כותרת התוכנית',
				 template: ''
			});	
		}
		else if ($scope.itemId == -1 && $scope.newimage =="")
		{
			$ionicPopup.alert({
				 title: 'יש לבחור תמונה',
				 template: ''
			});				
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			if ($scope.newimage)
				$scope.uploadimage = $scope.newimage;
			else
				$scope.uploadimage = $scope.fields.image;
			
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.Id,
				"title" : $scope.fields.title,
				"desc"  : $scope.fields.desc,
				"image" : $scope.uploadimage,
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/'+$scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				
				window.location ="#/app/categoriesplans";
				

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});					
		}
	}


	
})


.controller('BuildlistCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectNumber = $stateParams.ProjectId;
	
	//alert ($scope.projectId)
	
	$scope.GetBuildlist = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.ProjectNumber,
		};
		
		
		
		HostUrl = $rootScope.LaravelHost+'/getPlanList'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.PlansArray = data;
			$rootScope.PlansArray = data;
		   
		});
	}

	$scope.GetBuildlist();
	
	$scope.deleteBuild = function(id,index)
	{
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

								
			params = 
			{
				"user" : $localStorage.userid,
				"id" : id,
			};
			
			
			
			HostUrl = $rootScope.LaravelHost+'/deletePlanList'	
				
			SendPostToServer(params,HostUrl,function(data, success) 
			{
				$ionicLoading.hide();
				$scope.PlansArray.splice(index, 1);	
			   
			});
				
		} 
		 else 
		 {
		 }
	   });
	}
	
	$scope.choosePlan = function(id)
	{
		$rootScope.selectedPlanId = id;
		window.location ="#/app/selectbuild/"+$scope.ProjectNumber;
	}
	
})

.controller('SelectBuildCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectNumber = $stateParams.ProjectId;
	
	$scope.getPlans = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.ProjectNumber,
			"id" : $rootScope.selectedPlanId
		};
		

		HostUrl = $rootScope.LaravelHost+'/GetCatPlans'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.BuildArray = data;
			$rootScope.CatPlansArray = data;
			console.log("plans" , data)
		});
	}
	
	$scope.getPlans();
	
	
	$scope.selectCat = function(id,index)
	{
		//alert (index)
		window.location ="#/app/editplanimage/"+$scope.ProjectNumber+"/"+index;		
		//window.location ="#/app/buildcategories/"+$scope.ProjectNumber+"/"+id;	
	}
	
	
})	

.controller('EditPlanImageCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ProjectId;
	$scope.CatId = $stateParams.CatId;
	
	$scope.PlanData = $rootScope.CatPlansArray[$scope.CatId];
	$scope.planId = $scope.PlanData.index;
	
	//alert ($scope.planId)
	console.log("plan data" , $scope.PlanData)
	$scope.newimage = "";
	

	$scope.fields = 
	{
		"serverimage" : ""
	}
	
	
	if ($scope.PlanData.images)
	{
		if ($scope.PlanData.images[0].image)
		{
			$scope.fields.serverimage = $rootScope.serverHost+$scope.PlanData.images[0].image;
		}
	}
	
	
	
	//alert ( $scope.ProjectId)
	//alert ( $scope.CatId)
	//alert ($rootScope.selectedPlanId);
	
	
	



	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			
		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	
	
	
	$scope.savePlanImage= function()
	{
		if ($scope.newimage =="")
		{
			$ionicPopup.alert({
				 title: 'יש לבחור תמונה',
				 template: ''
			});				
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

								
			params = 
			{
				"user" : $localStorage.userid,
				"projectid" : $scope.ProjectId,
				"category" : $scope.planId,
				"selectedplan" : $rootScope.selectedPlanId,
				"image" : $scope.newimage,
			};
			
			console.log("save params:" , params)
			
			
			HostUrl = $rootScope.LaravelHost+'/savePlanImage'	
				
			SendPostToServer(params,HostUrl,function(data, success) 
			{
				$ionicLoading.hide();
				//alert (data);
				window.location ="#/app/selectbuild/"+$scope.ProjectId;
				

			});				
		}
	}


	
	//alert ($rootScope.selectedPlanId);

})

.controller('BuildCategoriesCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ProjectId;
	$scope.Id = $stateParams.Id;

	//alert ($scope.ProjectId)
	//alert ($scope.Id)
	
	
	$scope.getPlans = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"user" : $localStorage.userid,
			"projectid" : $scope.ProjectId,
			"category" : $scope.Id,
			"selectedplan" : $rootScope.selectedPlanId,
		};
		
		
		
		HostUrl = $rootScope.LaravelHost+'/GetPlans'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.BuildArray = data;
			$rootScope.PlansJson = data;

		});		
	}
	
	
	$scope.getPlans();
	
	
	$scope.PlanEdit = function(id)
	{
		//alert (id);
		
		window.location ="#/app/planmanage/"+$scope.ProjectId+"/"+$scope.Id+"/"+id;	
		
	}
	
	$scope.deletePlan = function(index,id)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeletePlan',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.BuildArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });
	}
	
})

.controller('PlanManageCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ProjectId;
	$scope.ItemId = $stateParams.ItemId;	
	$scope.Id = $stateParams.Id;
	$scope.PlanId = "";	
	
	
	//alert ($scope.ProjectId)
	//alert ($scope.ItemId)
	//alert ($scope.Id)
	//alert ($rootScope.selectedPlanId);

	
	$scope.fields = 
	{
		"name" : "",
		"desc" : "",
		"image" : "",
		"serverimage" : ""
	}
	
	if ($scope.Id == -1)
	{
		$scope.Url = "AddNewPlan";
	}
	else
	{
		$scope.Url = "EditPlan";
		
		if ($rootScope.PlansJson)
		{
			$scope.fields.name = $rootScope.PlansJson[$scope.Id].name;
			$scope.fields.desc = $rootScope.PlansJson[$scope.Id].desc;
			$scope.fields.image = $rootScope.PlansJson[$scope.Id].image;
			
			$scope.PlanId = $rootScope.PlansJson[$scope.Id].index;
			
			
			//alert ($scope.PlanId)
			
			if ($rootScope.PlansJson[$scope.Id].image)
			$scope.fields.serverimage = $rootScope.serverHost+$rootScope.PlansJson[$scope.Id].image;
	}
		}
	
	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}

	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			
		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }	

	$scope.savePlan = function()
	{
		
		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם התוכנית',
				 template: ''
			});				
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	
			
			if ($scope.newimage)
				$scope.serverimage = $scope.newimage;
			else
				$scope.serverimage = $scope.fields.image;
								
			params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.PlanId,
				"projectid" : $scope.ProjectId,
				"planid" : $rootScope.selectedPlanId,
				"category" : $scope.ItemId,

				"name" : $scope.fields.name,
				"desc" : $scope.fields.desc,
				"image" : $scope.serverimage,
			};
			
			
			
			HostUrl = $rootScope.LaravelHost+'/'+$scope.Url;	
				
			SendPostToServer(params,HostUrl,function(data, success) 
			{
				$ionicLoading.hide();


				window.location ="#/app/buildcategories/"+$scope.ProjectId+"/"+$scope.ItemId;
			   
			});			
		}
		
		

	}
	

})	


.controller('ManagePlanCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';
	$scope.ProjectId = $stateParams.ProjectId;
	$scope.Id = $stateParams.Id;
	$scope.PlanId = "";

	//alert ($scope.projectId)
	
	$scope.fields = 
	{
		"name" : ""
	}
	
	if ($scope.Id == -1)
	{
		$scope.Url = "newPlanList";
	}
	else
	{
		$scope.Url = "editPlanList";
		
		if ($rootScope.PlansArray)
		{
			$scope.fields.name = $rootScope.PlansArray[$scope.Id].name;
			$scope.PlanId = $rootScope.PlansArray[$scope.Id].index;
		}

		
	}
	
	$scope.savePlan = function()
	{
		if ($scope.fields.name =="")
		{
			$ionicPopup.alert({
				 title: 'יש להזין שם התוכנית',
				 template: ''
			});	
		}
		else
		{
			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

								
			params = 
			{
				"user" : $localStorage.userid,
				"id" : $scope.PlanId,
				"projectid" : $scope.ProjectId,
				"name":$scope.fields.name,
			};
			
			
			
			HostUrl = $rootScope.LaravelHost+'/'+$scope.Url;	
				
			SendPostToServer(params,HostUrl,function(data, success) 
			{
				$ionicLoading.hide();
				
				window.location ="#/app/buildlist/"+$scope.ProjectId;
			   
			});				
		}
	}
	

})


.controller('LeadsCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';

	$scope.getLeads = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{

		};

		HostUrl = $rootScope.LaravelHost+'/GetLeads'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.LeadsArray = data;
			console.log("leads", data);

		});	
	}
	
	$scope.getLeads();
	
	
	$scope.deleteLead = function(id,index)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'האם לאשר מחיקה?',
		 template: ''
	   });
	
	   confirmPopup.then(function(res) {
		if(res) 
		{

			$ionicLoading.show({
			  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
			});	

						
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			send_params = 
			{
				"user" : $localStorage.userid,
				"id" : id
			}
			//console.log(login_params)
			$http.post($rootScope.LaravelHost+'/DeleteLead',send_params)
			.success(function(data, status, headers, config)
			{
				
				$ionicLoading.hide();
				$scope.LeadsArray.splice(index, 1);	
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});	
				
		} 
		 else 
		 {
		 }
	   });		
	}
})


.controller('ManageAboutCtrl', function($scope,$stateParams,$localStorage,$ionicPopup,$http,$rootScope,$ionicModal,$ionicLoading,$timeout,$cordovaCamera,SendPostToServer,$sce) 
{
	$scope.navTitle ='<img class="title-image" src="img/logo.png" />';

	$scope.fields = 
	{
		"image"  : "",
		"serverimage" : "",
		"desc" : ""
	}
	
	
	
	$scope.pictureChoser = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: 'בחר/י מקור התמונה',
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: 'מצלמה',
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: 'גלריית תמונות',
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   
		{
		text: 'ביטול',
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	  
	   ]
	  });
	}
	
	// destinationType : Camera.DestinationType.DATA_URL,  CAMERA
	$scope.takePicture = function(index) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 300,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.LaravelHost+'/uploadImage'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {

				//alert (111)
				//alert (data.response)
				$scope.newimage = data.response;
				$scope.fields.serverimage = $rootScope.serverHost+data.response;
				//alert ($scope.fields.serverimage)
				//alert ($scope.uploadedimage)
				//$scope.showimage = true;

				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }		
	
	
	$scope.getAbout = function()
	{
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{

		};

		HostUrl = $rootScope.LaravelHost+'/GetAbout'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			$scope.AboutData = data[0];
			
			
			$scope.fields.desc = $scope.AboutData.desc;
			
			if ($scope.AboutData.image)
			{
				$scope.fields.image = $scope.AboutData.image;
				$scope.fields.serverimage = $rootScope.serverHost+$scope.AboutData.image;
			}
				
			
			console.log($scope.AboutData);

		});	
	}
	
	$scope.getAbout();
	
	$scope.saveAbout = function()
	{
		if ($scope.newimage)
			$scope.serverimage = $scope.newimage;
		else
			$scope.serverimage = $scope.fields.image;
		

		
		$ionicLoading.show({
		  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
		});	

							
		params = 
		{
			"image" : $scope.serverimage,
			"desc" :  $scope.fields.desc
		};

		HostUrl = $rootScope.LaravelHost+'/SaveAbout'	
			
		SendPostToServer(params,HostUrl,function(data, success) 
		{
			$ionicLoading.hide();
			
			$ionicPopup.alert({
				 title: 'נשמר בהצלחה',
				 template: ''
			});				


		});	


		
		
	}
	

})

	

.filter('html', ['$sce', function ($sce) { 
    return function (text) {
        return $sce.trustAsHtml(text);
    };    
}])
