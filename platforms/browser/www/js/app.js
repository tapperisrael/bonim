// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers','ngStorage','ngCordova','google.places','starter.factories'])

/*.config(function($ionicConfigProvider) {
  $ionicConfigProvider.scrolling.jsScrolling(true);
})*/

.run(function($ionicPlatform,$rootScope,$http,$localStorage, $window) {

$rootScope.LaravelHost = 'http://tapper.co.il/ibuild/bonim/public/';
$rootScope.serverHost = 'http://tapper.co.il/ibuild/php/';
$rootScope.buildingId = '';
$rootScope.ProjectsJson = [];
$rootScope.MifratDataArray = [];
$rootScope.MifratArray = [];
$rootScope.ApartmentsArray = [];
$rootScope.AppUsersArray = [];
$rootScope.currentProject = [];
$rootScope.projectImage = "";
$rootScope.chatProjects = [];	
$rootScope.currentProjectName = "";
$rootScope.currentProjectAddress = "";
$rootScope.currentApartment = "";
$rootScope.selectedMifratList = "";
$rootScope.selectedProjectNumber = "";
$rootScope.selectedPlanId = "";
$rootScope.MifratListArray = [];
$rootScope.PlansCatArray = [];
$rootScope.PlansArray = [];
$rootScope.PlansJson = [];
$rootScope.BuildDataArray = [];
$rootScope.CatPlansArray = [];

console.log("Enter")
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false).text('');
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })



	.state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        }
      }
    })
	.state('app.projects', {
      url: '/projects',
      views: {
        'menuContent': {
          templateUrl: 'templates/projects.html',
          controller: 'ProjectCtrl'
        }
      }
    })
	
	.state('app.manageproject', {
      url: '/manageproject/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageproject.html',
          controller: 'ManageProjectCtrl'
        }
      }
    })

	
	.state('app.chatProject', {
      url: '/chatProject',
      views: {
        'menuContent': {
          templateUrl: 'templates/chatProject.html',
          controller: 'chatProjectCtrl'
        }
      }
    })
	
	.state('app.buildings', {
      url: '/buildings/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/buildings.html',
          controller: 'BuildingCtrl'
        }
      }
    })

	
	.state('app.editplanimage', {
      url: '/editplanimage/:ProjectId/:CatId',
      views: {
        'menuContent': {
          templateUrl: 'templates/editplanimage.html',
          controller: 'EditPlanImageCtrl'
        }
      }
    })

	

	.state('app.managebuildings', {
      url: '/managebuildings/:ProjectId/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/managebuildings.html',
          controller: 'ManageBuildingsCtrl'
        }
      }
    })	


	.state('app.apartments', {
      url: '/apartments/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/apartments.html',
          controller: 'ApartmentsCtrl'
        }
      }
    })

	.state('app.manageapartments', {
      url: '/manageapartments/:BuildingId/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageapartments.html',
          controller: 'ManageApartmentsCtrl'
        }
      }
    })

	


	
	

	
	
	.state('app.editpartment', {
      url: '/editpartment/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/editpartment.html',
          controller: 'EditApartmentCtrl'
        }
      }
    })

	

	.state('app.apartmentdetails', {
	  cache:false,
      url: '/apartmentdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/apartmentdetails.html',
          controller: 'ApartmentsDetailsCtrl',
		  //cache:false
        }
      }
    })
	

	
	.state('app.adminmanage', {
      url: '/adminmanage',
      views: {
        'menuContent': {
          templateUrl: 'templates/adminmanage.html',
          controller: 'AdminManageCtrl'
        }
      }
    })	
		
	.state('app.categoriesmifrat', {
      url: '/categoriesmifrat',
      views: {
        'menuContent': {
          templateUrl: 'templates/categoriesmifrat.html',
          controller: 'CategoriesMifratCtrl'
        }
      }
    })	


	.state('app.categoriesplans', {
      url: '/categoriesplans',
      views: {
        'menuContent': {
          templateUrl: 'templates/categoriesplans.html',
          controller: 'CategoriesPlansCtrl'
        }
      }
    })	

	
	.state('app.manageplancat', {
      url: '/manageplancat/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageplancat.html',
          controller: 'ManagePlanCatCtrl'
        }
      }
    })	

	
	


	
	.state('app.managemifratcat', {
      url: '/managemifratcat/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/managemifratcat.html',
          controller: 'ManageMifratCatCtrl'
        }
      }
    })	
	
	
	.state('app.mifratgallery', {
      url: '/mifratgallery/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/mifratgallery.html',
          controller: 'MifratGalleryCtrl'
        }
      }
    })	
		
	
	.state('app.buildmifrat', {
      url: '/buildmifrat/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/buildmifrat.html',
          controller: 'BuildMifratCtrl'
        }
      }
    })	

	.state('app.selectmifrat', {
      url: '/selectmifrat/:ProjectId',
      views: {
        'menuContent': {
          templateUrl: 'templates/selectmifrat.html',
          controller: 'SelectMifratCtrl'
        }
      }
    })		

	.state('app.selectbuild', {
      url: '/selectbuild/:ProjectId',
      views: {
        'menuContent': {
          templateUrl: 'templates/selectbuild.html',
          controller: 'SelectBuildCtrl'
        }
      }
    })	
	
	
	.state('app.projectsettings', {
      url: '/projectsettings/:ProjectId',
      views: {
        'menuContent': {
          templateUrl: 'templates/projectsettings.html',
          controller: 'ProjectSettings'
        }
      }
    })


	.state('app.mifratlist', {
      url: '/mifratlist/:ProjectId',
      views: {
        'menuContent': {
          templateUrl: 'templates/mifratlist.html',
          controller: 'MifratListCtrl'
        }
      }
    })

	.state('app.buildlist', {
      url: '/buildlist/:ProjectId',
      views: {
        'menuContent': {
          templateUrl: 'templates/buildlist.html',
          controller: 'BuildlistCtrl'
        }
      }
    })
	
	.state('app.manageplan', {
      url: '/manageplan/:ProjectId/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageplan.html',
          controller: 'ManagePlanCtrl'
        }
      }
    })
		
	
	
	
	
	.state('app.managemifratlist', {
      url: '/managemifratlist/:ProjectId/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/managemifratlist.html',
          controller: 'ManageMifratList'
        }
      }
    })	

	
	
	.state('app.mifratmanage', {
      url: '/mifratmanage/:ProjectId/:ItemId/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/mifratmanage.html',
          controller: 'MifratManageCtrl'
        }
      }
    })	

	
	.state('app.planmanage', {
      url: '/planmanage/:ProjectId/:ItemId/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/planmanage.html',
          controller: 'PlanManageCtrl'
        }
      }
    })	

	
	

	.state('app.mifratcategories', {
      url: '/mifratcategories/:ProjectId/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/mifratcategories.html',
          controller: 'BuildMifratCtrl'
		  //controller: 'MifratCategoriesCtrl'  
        }
      }
    })	

	.state('app.buildcategories', {
      url: '/buildcategories/:ProjectId/:Id',
      views: {
        'menuContent': {
          templateUrl: 'templates/buildcategories.html',
          controller: 'BuildCategoriesCtrl'
        }
      }
    })		

	
	.state('app.managemifrat', {
      url: '/managemifrat/:ProjectNumber/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/managemifrat.html',
          controller: 'ManageMifratCtrl'
        }
      }
    })	
		
	.state('app.customaparment', {
      url: '/customaparment/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/customaparment.html',
          controller: 'CustomAparmentCtrl'
        }
      }
    })	

	.state('app.manageaparment', {
      url: '/manageaparment/:Project/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageaparment.html',
          controller: 'ManageAparmentCtrl'
        }
      }
    })	

	.state('app.aparmentusers', {
      url: '/aparmentusers/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/aparmentusers.html',
          controller: 'AparmentUsersCtrl'
        }
      }
    })			
	
	
	.state('app.manageappusers', {
      url: '/manageappusers/:ApparmentId/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageappusers.html',
          controller: 'ManageAppusersCtrl'
        }
      }
    })		


	.state('app.mifratcat', {
      url: '/mifratcat/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/mifratcat.html',
          controller: 'MifratCatCtrl'
        }
      }
    })			

	.state('app.plancat', {
      url: '/plancat/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/plancat.html',
          controller: 'PlanCatCtrl'
        }
      }
    })	
	
	.state('app.apartmentplans', {
      url: '/apartmentplans/:PlanList/:Category',
      views: {
        'menuContent': {
          templateUrl: 'templates/apartmentplans.html',
          controller: 'ApartmentPlansCtrl'
        }
      }
    })		
	
	
	
	
	
	.state('app.apartmentmifrat', {
      url: '/apartmentmifrat/:MifratList/:Category',
      views: {
        'menuContent': {
          templateUrl: 'templates/apartmentmifrat.html',
          controller: 'ApartmentMifratCtrl'
        }
      }
    })		
	
	.state('app.mifratdetails', {
      url: '/mifratdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/mifratdetails.html',
          controller: 'MifratDetailsCtrl'
        }
      }
    })	

	
	.state('app.builddetails', {
      url: '/builddetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/builddetails.html',
          controller: 'BuildDetailsCtrl'
        }
      }
    })	

	
	
	.state('app.categorygallery', {
      url: '/categorygallery/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/categorygallery.html',
          controller: 'CategoryGalleryCtrl'
        }
      }
    })	

	
	.state('app.chatPage', {
      url: '/chatPage/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/chatPage.html',
          controller: 'chatPageCtrl'
        }
      }
    })	
	
	.state('app.EditChatProject', {
      url: '/EditChatProject/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/EditChatProject.html',
          controller: 'EditChatProjectCtrl'
        }
      }
    })	
	
	.state('app.chatDetails', {
      url: '/chatDetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/chatDetails.html',
          controller: 'chatDetailsCtrl'
        }
      }
    })	


	.state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AboutCtrl'
        }
      }
    })	

	.state('app.projectdetails', {
      url: '/projectdetails/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/projectdetails.html',
          controller: 'ProjectDetailsCtrl'
        }
      }
    })	

	.state('app.leads', {
      url: '/leads',
      views: {
        'menuContent': {
          templateUrl: 'templates/leads.html',
          controller: 'LeadsCtrl'
        }
      }
    })	

	.state('app.manageabout', {
      url: '/manageabout',
      views: {
        'menuContent': {
          templateUrl: 'templates/manageabout.html',
          controller: 'ManageAboutCtrl'
        }
      }
    })	
	
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});



















